# Newspaper dokuwiki template

* Based on a wordpress theme that is no longer listed in the Wordpress Theme Directory for being outated (use the Wayback Machine)
* This is also a [pmwiki theme](https://www.pmwiki.org/wiki/Skins/NewsPaper)
* Designed by [The Undersigned](https://archive.vn/FyW0A)
* Converted by [desbest](http://desbest.com)
* Metadata is in template.info.txt
* Under the GPL license (see copying file)
* [More information](http://dokuwiki.org/template:newspaper)

![newspaper theme screenshot](https://i.imgur.com/BeZCtCC.png)