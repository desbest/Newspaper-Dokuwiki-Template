<?php
/**
 * DokuWiki Newspaper Template
 * Based on the starter template and a wordpress theme of the same name
 *
 * @link     http://dokuwiki.org/template:newspaper
* @author   desbest <afaninthehouse@gmail.com>
 * @license  GPL 2 (http://www.gnu.org/licenses/gpl.html)
 */

if (!defined('DOKU_INC')) die(); /* must be run from within DokuWiki */
@require_once(dirname(__FILE__).'/tpl_functions.php'); /* include hook for template functions */

$showTools = !tpl_getConf('hideTools') || ( tpl_getConf('hideTools') && !empty($_SERVER['REMOTE_USER']) );
$showSidebar = page_findnearest($conf['sidebar']) && ($ACT=='show');
$sidebarElement = tpl_getConf('sidebarIsNav') ? 'nav' : 'aside';
?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $conf['lang'] ?>"
  lang="<?php echo $conf['lang'] ?>" dir="<?php echo $lang['direction'] ?>" class="no-js">
<head>
    <meta charset="UTF-8" />
    <title><?php tpl_pagetitle() ?> [<?php echo strip_tags($conf['title']) ?>]</title>
    <script>(function(H){H.className=H.className.replace(/\bno-js\b/,'js')})(document.documentElement)</script>
    <?php tpl_metaheaders() ?>
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <?php echo tpl_favicon(array('favicon', 'mobile')) ?>
    <?php tpl_includeFile('meta.html') ?>
    <script defer type="text/javascript">
    //inline javascript doesn't work on 2020 hogfather
    //jQuery(document).ready(function() {
        // jQuery("#dw__toc").prependTo("#column3");
        // jQuery("#dw__toc").addClass("tocmovedtocolumn");
    //});
    </script>
    <script defer type="text/javascript" src="<?php echo tpl_basedir();?>/mobileinputsize.js"></script>
</head>

<body>


<div id="wrapper" class="site <?php echo tpl_classes(); ?> <?php echo ($showSidebar) ? 'hasSidebar' : ''; ?>">
<div id="header">
<h1><?php tpl_link(wl(),$conf['title'],'accesskey="h" title="[H]"') ?></h1>
    <?php /* how to insert logo instead (if no CSS image replacement technique is used):
    upload your logo into the data/media folder (root of the media manager) and replace 'logo.png' accordingly:    
    tpl_link(wl(),'<img src="'.ml('logo.png').'" alt="'.$conf['title'].'" />','id="dokuwiki__top" accesskey="h" title="[H]"') */ ?>
    <ul class="a11y skip">
        <li><a href="#dokuwiki__content"><?php echo $lang['skip_to_content'] ?></a></li>
    </ul>
<?php if ($conf['tagline']): ?>
    <h2 class="slogan"><?php echo $conf['tagline'] ?></h2>
<?php endif ?>
</div>



<div id="column1">
<h3 class="time">Site Tools</h3>
<!-- SITE TOOLS -->
<nav id="" aria-labelledby="dokuwiki__sitetools_heading">
    <h3 class="a11y" id="dokuwiki__sitetools_heading"><?php echo $lang['site_tools'] ?></h3>
    
    <?php
        // mobile menu (combines all menus in one dropdown)
        // if (file_exists(DOKU_INC . 'inc/Menu/MobileMenu.php')) {
        //     echo (new \dokuwiki\Menu\MobileMenu())->getDropdown($lang['tools']);
        // } else {
        //   tpl_actiondropdown($lang['tools']);
        // }
    ?>
    <ul>
        <?php if (file_exists(DOKU_INC . 'inc/Menu/SiteMenu.php')) {
            echo (new \dokuwiki\Menu\SiteMenu())->getListItems('action ', false);
        } else {
            _tpl_sitetools();
        } ?>
    </ul>
</nav>

<!-- USER TOOLS -->
<?php if ($conf['useacl'] && $showTools): ?>
    <h3 class="time">User Tools</h3>
    <nav id="" aria-labelledby="dokuwiki__usertools_heading">
        <h3 class="a11y" id="dokuwiki__usertools_heading"><?php echo $lang['user_tools'] ?></h3>
        <ul>
            <?php if (!empty($_SERVER['REMOTE_USER'])) {
                echo '<li class="user">';
                tpl_userinfo(); /* 'Logged in as ...' */
                echo '</li>';
            } ?>
            <?php if (file_exists(DOKU_INC . 'inc/Menu/UserMenu.php')) {
                /* the first parameter is for an additional class, the second for if SVGs should be added */
                echo (new \dokuwiki\Menu\UserMenu())->getListItems('action ', false);
            } else {
                /* tool menu before Greebo */
                _tpl_usertools();
            } ?>
        </ul>
    </nav>
<?php endif ?>
<!--wiki:$SiteGroup.SideBar $Group.SideBar-->
<h3 class="sidehead"><a href="$ScriptUrl/$SiteGroup/Search">Search</a></h3>
<!-- <form  class='wikisearch' action='$PageUrl' method='get'>
<ul>
<li>
    <input type='text' name='q' value='' size='12' />
    <input type='hidden' name='action' value='search' />
</li>
</ul>
</form> -->
<?php tpl_searchform() ?>
</div>

<div id="column2">

 <!-- BREADCRUMBS -->
<?php if($conf['breadcrumbs']){ ?>
    <div class="breadcrumbs"><?php tpl_breadcrumbs() ?></div>
<?php } ?>
<?php if($conf['youarehere']){ ?>
    <div class="breadcrumbs"><?php tpl_youarehere() ?></div>
<?php } ?>

<?php html_msgarea() /* occasional error and info messages on top of the page */ ?>

<!-- <h2><a href='$PageUrl'>$Title</a></h2> -->
<!-- ********** CONTENT ********** -->
<main id="dokuwiki__content"><!-- <div class="pad"> -->
    <?php tpl_flush() /* flush the output buffer */ ?>
    <?php tpl_includeFile('pageheader.html') ?>

    <div class="page">
        <!-- wikipage start -->
        <?php tpl_content() /* the main content */ ?>
        <!-- wikipage stop -->
        <div class="clearer"></div>
    </div>

    <?php tpl_flush() ?>
    <?php tpl_includeFile('pagefooter.html') ?>
<!-- </div> --></main><!-- /content -->
<!--PageText-->
</div>

<div id="column3">

<h3 class="sidehead"><a href="$ScriptUrl/$SiteGroup/PageActions">Page Tools</a></h3>
<!-- PAGE ACTIONS -->
<?php if ($showTools): ?>
    <nav id="" aria-labelledby="dokuwiki__pagetools_heading">
        <h3 class="a11y" id="dokuwiki__pagetools_heading"><?php echo $lang['page_tools'] ?></h3>
        <ul>
            <?php if (file_exists(DOKU_INC . 'inc/Menu/PageMenu.php')) {
                echo (new \dokuwiki\Menu\PageMenu())->getListItems('action ', false);
            } else {
                _tpl_pagetools();
            } ?>
        </ul>
    </nav>
<?php endif; ?>

<!-- ********** ASIDE ********** -->
<?php if ($showSidebar): ?>
    <<?php echo $sidebarElement ?> id="writtensidebar" aria-label="<?php echo $lang['sidebar'] ?>"><!-- <div class="pad aside include group"> -->
        <?php tpl_includeFile('sidebarheader.html') ?>
        <?php tpl_include_page($conf['sidebar'], 1, 1) /* includes the nearest sidebar page */ ?>
        <?php tpl_includeFile('sidebarfooter.html') ?>
        <div class="clearer"></div>
    </<?php echo $sidebarElement ?>>
    <!-- </div> --><!-- </div> --><!-- /aside -->
<?php endif; ?>
</div>


<div id="footer">
<p>Design by <a href="https://archive.vn/FyW0A">theundersigned</a> | converted by <a href="http://desbest.com">desbest</a></p>
<div class="doc"><?php tpl_pageinfo() /* 'Last modified' etc */ ?></div>
<?php tpl_license('button') /* content license, parameters: img=*badge|button|0, imgonly=*0|1, return=*0|1 */ ?>

<?php tpl_includeFile('footer.html') ?>
</div>


    <?php /* the "dokuwiki__top" id is needed somewhere at the top, because that's where the "back to top" button/link links to */ ?>
    <?php /* tpl_classes() provides useful CSS classes; if you choose not to use it, the 'dokuwiki' class at least
             should always be in one of the surrounding elements (e.g. plugins and templates depend on it) */ ?>
    <div id="dokuwiki__top">
    

      
    </div><!-- /site -->

    <div class="no"><?php tpl_indexerWebBug() /* provide DokuWiki housekeeping, required in all templates */ ?></div>
</body>
</html>
